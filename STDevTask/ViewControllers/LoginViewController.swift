//
//  LoginViewController.swift
//  STDevTask
//
//  Created by Hayk Aslanyan on 8/16/18.
//  Copyright © 2018 Hayk Aslanyan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.passwordTextField {
            textField.resignFirstResponder()
        }else if textField == self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        }
        return false
    }
    
    // MARK: Actions

    @IBAction func loginBtnAction(_ sender: Any) {
        if self.emailTextField.text == "test@test.com" && self.passwordTextField.text == "password" {
            UserDefaults.standard.set(true, forKey: "userIsSignIn")
            let navVC:UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationViewController") as! UINavigationController
            navVC.modalTransitionStyle = .flipHorizontal
            self.present(navVC, animated: true, completion: nil)
        }else{
            let alertController = UIAlertController(title: "Error", message: "Wrong email or password.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func closeKeyboardAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func ShowPasswordAction(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "eye") {
            self.passwordTextField.isSecureTextEntry = false
            sender.setImage(UIImage(named: "hideeye"), for: .normal)
        }else{
            self.passwordTextField.isSecureTextEntry = true
            sender.setImage(UIImage(named: "eye"), for: .normal)
        }
    }
    
}
