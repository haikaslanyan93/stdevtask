//
//  FilterViewController.swift
//  STDevTask
//
//  Created by Hayk Aslanyan on 8/21/18.
//  Copyright © 2018 Hayk Aslanyan. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var categoriesTableView: UITableView!
    let categoryArray = ["Books", "Sporting Goods", "Music", "Travel", "Electronics", "Other"]
    var selectedCategories:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FilterCategoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FilterCategoriesTableViewCell") as! FilterCategoriesTableViewCell
        for str in self.selectedCategories{
            let category:String = str as! String
            if category == self.categoryArray[indexPath.row]{
                cell.cellIsSelected = true
                cell.contentView.backgroundColor = UIColor.gray
                break
            }
        }
        cell.categoryName.text = self.categoryArray[indexPath.row]
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:FilterCategoriesTableViewCell = self.categoriesTableView.cellForRow(at: indexPath) as! FilterCategoriesTableViewCell
        if cell.cellIsSelected{
            self.selectedCategories.remove(self.categoryArray[indexPath.row])
            cell.contentView.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0)
        }else{
            self.selectedCategories.add(self.categoryArray[indexPath.row])
            cell.contentView.backgroundColor = UIColor.gray
        }
        cell.cellIsSelected = !cell.cellIsSelected
    }
    
    // MARK: Actions
    
    @IBAction func filterBtnAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FilterCategoriesSelected"), object: nil, userInfo: ["categories" : self.selectedCategories])
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
