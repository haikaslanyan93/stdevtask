//
//  ViewController.swift
//  STDevTask
//
//  Created by Hayk Aslanyan on 8/15/18.
//  Copyright © 2018 Hayk Aslanyan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
   
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var productTableView: UITableView!
    var filteredProducts:NSMutableArray = NSMutableArray()
    var searchedProducts:NSMutableArray = NSMutableArray()
    var allProducts:NSMutableArray = NSMutableArray()
    var selectedCategories:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        WebServiceManager.getProducts() { (json, response, error) in
            if json != nil {
                let swiftyJsonVar = JSON(json!)
                for subjson in swiftyJsonVar.arrayValue{
                    let product:Product = Product(withJSON: subjson)
                    self.allProducts.add(product)
                }
                
                self.searchedProducts = NSMutableArray(array: self.allProducts)
                self.filteredProducts = NSMutableArray(array: self.allProducts)
                self.productTableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(filterCategoriesConfirmed), name: NSNotification.Name(rawValue: "FilterCategoriesSelected"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FilterCategoriesSelected"), object: nil)
    }
    
    @objc func filterCategoriesConfirmed(notification: Notification) -> Void{
        self.selectedCategories = NSMutableArray(array: notification.userInfo!["categories"] as! NSArray)
        if selectedCategories.count == 0{
            self.searchedProducts = NSMutableArray(array: self.allProducts)
        }else{
            let filteredProducts:NSMutableArray = NSMutableArray()
            for str in self.selectedCategories{
                let categoryName:String = str as! String
                for prod in self.allProducts {
                    let product:Product = prod as! Product
                    let name:String = product.categoryName
                    if name == categoryName{
                        filteredProducts.add(prod)
                    }
                }
            }
            self.searchedProducts = filteredProducts
        }
        self.productTableView.reloadData()
        self.filteredProducts = NSMutableArray(array: self.searchedProducts)
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (self.searchBar.text?.isEmpty)! {
            self.searchedProducts = NSMutableArray(array: self.filteredProducts)
        }else{
            let searchProducts:NSMutableArray = NSMutableArray()
            for prod in self.searchedProducts {
                let product:Product = prod as! Product
                if product.name.lowercased().hasPrefix((self.searchBar.text?.lowercased())!){
                    searchProducts.add(prod)
                }
            }
            self.searchedProducts = NSMutableArray(array: searchProducts)
        }
        self.productTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar{
            searchBar.resignFirstResponder()
        }
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchedProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ProductsTableViewCell = self.productTableView.dequeueReusableCell(withIdentifier: "ProductsTableViewCell") as! ProductsTableViewCell
        let product:Product = self.searchedProducts[indexPath.row] as! Product
        cell.productName.text = product.name
        cell.categoryName.text = product.categoryName
        cell.price.text = String(product.price)
            
        let imgUrl = AppURL.productImagePath + product.profileImage
        Alamofire.request(imgUrl, method: .get).responseData { response in
            if response.error == nil {
                if let data = response.data {
                    cell.productImage.image = UIImage(data: data)
                }
            }
        }
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    // MARK: - Actions
    
    @IBAction func searchAction(_ sender: UIButton) {
        self.searchBar.becomeFirstResponder()
    }
    
    @IBAction func sortAction(_ sender: UIButton) {
        self.show()
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
        let filterVC:FilterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        filterVC.selectedCategories = NSMutableArray(array: self.selectedCategories)
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        logout()
    }
    
    // MARK: Functions
    
    func logout(){
        UserDefaults.standard.removeObject(forKey: "userIsSignIn")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.performUserFirstLoginCheckAction()
    }
    
    func show() {
        let actionSheet = UIAlertController(title: "Sort", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "By name", style: .default, handler: { (action) in
            let sortedProducts = self.searchedProducts.sorted{($0 as! Product).name < ($1 as! Product).name}
            self.searchedProducts = NSMutableArray(array: sortedProducts)
            self.productTableView.reloadData()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "By price", style: .default, handler: { (action) in
            let sortedProducts = self.searchedProducts.sorted{($0 as! Product).price < ($1 as! Product).price}
            self.searchedProducts = NSMutableArray(array: sortedProducts)
            self.productTableView.reloadData()
        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
}

