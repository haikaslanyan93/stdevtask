//
//  Product.swift
//  STDevTask
//
//  Created by Hayk Aslanyan on 8/18/18.
//  Copyright © 2018 Hayk Aslanyan. All rights reserved.
//

import UIKit
import SwiftyJSON

class Product: NSObject {
    var name = ""
    var profileImage = ""
    var categoryName = ""
    var productId = ""
    var price = 0
    
    convenience init(withJSON json: JSON) {
        self.init()
        
        if let name = json["name"].string {
            self.name = name
        }
        if let productId = json["_id"].string {
            self.productId = productId
        }
        if let profileImage = json["image"].array {
            self.profileImage = profileImage[0].string!
        }
        if let categoryName = json["category_name"].string {
            self.categoryName = categoryName
        }
        if let price = json["price"].number {
            self.price = price.intValue
        }
    }

    func getDict() -> Dictionary<String, Any> {
        let   dict = ["name" : self.name,
                      "categoryName" : self.categoryName,
                      "profileImage" : self.profileImage,
                      "price" : String(self.price),
                      "_id" : self.productId]
        return dict
    }

}
