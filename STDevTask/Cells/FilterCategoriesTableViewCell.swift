//
//  FilterCategoriesTableViewCell.swift
//  STDevTask
//
//  Created by Hayk Aslanyan on 8/21/18.
//  Copyright © 2018 Hayk Aslanyan. All rights reserved.
//

import UIKit

class FilterCategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    var cellIsSelected:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
