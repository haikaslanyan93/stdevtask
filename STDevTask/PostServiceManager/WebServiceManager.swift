//
//  WebServiceManager.swift
//  STDevTask
//
//  Created by Hayk Aslanyan on 8/21/18.
//  Copyright © 2018 Hayk Aslanyan. All rights reserved.
//

import Foundation
import Alamofire

final class WebServiceManager {
    
    class func getProducts( _ completion: @escaping (Any?, URLResponse?, String?) -> Void) {
        let headers = ["content-type":"application/json", "x-apikey":"1da5e084b279e4053c5a61163661185b77467"]
        sendRequest(AppURL.getProducts, headers, .get, nil, completion: completion)
    }
    
    //MARK: - Private Methods

    class private func sendRequest(_ url: String, _ headerFields: [String:String], _ method: HTTPMethod, _ param: [String:Any]?, completion: @escaping (Any?, URLResponse?, String?) -> Void ) {
        sendRequestAlamofire(url, headerFields, method, param, completion: completion)
        return
    }
    
    
    class private func sendRequestAlamofire(_ url: String, _ headerFields: [String:String], _ method: HTTPMethod, _ param: [String:Any]?, completion: @escaping (Any?, URLResponse?, String?) -> Void ) {
        
        Alamofire.request(url, method: method, parameters: param, encoding: JSONEncoding.default, headers:headerFields).responseJSON{ (responce) in
            switch(responce.result) {
            case .success(_):
                if responce.result.isSuccess == true {
                    DispatchQueue.main.async(execute: {
                        completion(responce.result.value, responce.response, nil)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        completion(nil, responce.response, "Error")
                    })}
                break
            case .failure(_):
                DispatchQueue.main.async(execute: {
                    completion(nil, responce.response, "Error")
                })
                break
            }
        }
    }
}
