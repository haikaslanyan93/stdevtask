//
//  AppURL.swift
//  STDevTask
//
//  Created by Hayk Aslanyan on 8/21/18.
//  Copyright © 2018 Hayk Aslanyan. All rights reserved.
//

import Foundation

struct AppURL {
    
    private struct BaseURL {
        static let baseURL = "https://stdevtask-7bad.restdb.io/"
    }
    
    private struct UserURL {
        static let getProducts = "rest/product"
        static let imagePath = "media/"
    }
    
    // MARK: - URL's
    
    static var getProducts: String {
        return BaseURL.baseURL + UserURL.getProducts
    }
    
    static var productImagePath: String {
        return BaseURL.baseURL + UserURL.imagePath
    }
}
